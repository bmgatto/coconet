#!/bin/bash
# A script written by Brandon Gatto
# to allow access to the wonderful coconet server

#Coconet commands
#    coconet install - Installs the Coconet system
#    coconet put <FILE> - Upload file to Coconet
#    coconet get <FILE> - Download File from Coconet
#    coconet ftp - Enter FTP for Coconet
#    coconet ssh - Enter SSH for Coconet
#    coconet schedule - Set up automatic uploading

# Check to make sure Rsync is installed
# The script will not work properly without it
if [ ! `which rsync` ]
then
    echo "This script requires rsync to run properly."
    echo "Please install rsync"
    exit 1
fi

function resetInstall {
    # This clears the existing config file for a fresh install
    mkdir -p ~/.config/coconet
    echo > ~/.config/coconet/coconet.conf
}

function userSetup {
    # If the  user already has an account they can enter their credentials for login
    echo "Do you already have an account with the Coconet?"
    read -p "(y/N) " userInputAccount
    if [[ $userInputAccount = "y" ]] #TODO Get this to match upper and lowercase
    then
        echo
        username=""
        while [[ $username = "" ]]
        do
            echo "Please enter your username and password:"
            read -p "Username: " username
        done
        #TODO ssh-keygen to save password
        echo "USERNAME=$username" >> ~/.config/coconet/coconet.conf
        ssh-keygen -f ~/.ssh/id_rsa -t rsa
        ssh-copy-id -i ~/.ssh/id_rsa $username@coconet.fun

#    else
#        # Otherwise they will be allowed to create a new account
#        echo
#        echo "Would you like to create an account now?"
#        echo "(Note that if you choose not to create an account then you will be "
#        echo "allowed to use a guest account for temporary storage)"
#        read -p "(y/N) " userInputCreate
#        if [[ $userInputCreate = "y" ]]
#        then
#            echo
#            echo "A user account will be created for you, please stand by"
#            #TODO Create user account
#            exit 1
#        else
#            # Or install as a guest, file upload is still allowed, but
#            # it may be deleted if more room is needed
#            echo
#            echo "Your account will be set up as a guest"
#            echo "Stored files will be deleted if room is needed"
#            echo "ACCOUNT=guest" >> ~/.config/coconet/coconet.conf
#            #TODO Set up ssh keygen for password saving
#            exit 0
#        fi
    fi
}

function scheduleSync {
    # A folder can be specified to syncronize with the server
    # Works just like dropbox
    echo
    # At the moment the folder can only be a descendant of the users home
    # potentially change that in the future
    echo "Which folder would you like to synchronize automatically? (~/Coconut)"
    read -p "~/" userInputSyncFolder
    # If the user does not supply a folder then the default will be used
    if [[ $userInputSyncFolder = "" ]]
    then
        coconutFolder="$HOME/Coconut"
    else
        coconutFolder="$HOME/$userInputSyncFolder"
    fi

    # Saves and creates the sync folder
    echo "COCONUT=$coconutFolder" >> ~/.config/coconet/coconet.conf
    mkdir -p $coconutFolder
    # Creates a script that will copy the files from the remote dir to the local dir
    # and vice versa, making the contents of both the same
    echo '# Automatically configured by the Coconut config thing' > $coconutFolder/rsync
    # Creates the coconut folder on the remote machine
    ssh $username@coconet.fun "mkdir -p $coconutFolder"
    echo "rsync -avP $coconutFolder/ $username@coconet.fun:$coconutFolder" >> $coconutFolder/rsync
    echo "rsync -avP $username@coconet.fun:$coconutFolder/ $coconutFolder" >> $coconutFolder/rsync
    #bash $coconutFolder/rsync

    #TODO set up sync with cron
    (crontab -l; echo -e "*/10 * * * * /bin/bash $coconutFolder/rsync\n" ) | crontab -
    echo "Automatic synchronization has been configured"
}

# The main if statement
if [[ $1 = install ]]
then
    # Clears the installation
    resetInstall

    echo "Welcome to the Coconet Interactive Installer"
    echo "For ease of use, please place this script somewhere in the \$PATH"
    # Sets up the user account
    userSetup

    echo
    # Sets up sync
    echo "Would you like to configure automatic synchronization?"
    read -p "(y/N) " userInputAutosync
    if [[ $userInputAutosync = "y" ]]
    then
        scheduleSync
    else
        echo "No files will be automatically uploaded"
    fi

    echo
    echo "The coconet has now been fully configured"
    echo "If you would like to start over, run coconet install again"
    echo "If you have any questions, type coconet help"

# File Upload script
elif [[ $1 = put ]]
then
    if [ $# -ge 2 ]
    then
        # If given a file it will upload it to the server
        # an optional destination can also be provided
        rsync -avP $2 `grep USERNAME ~/.config/coconet/coconet.conf | cut -d= -f2`@coconet.fun:$3
    else
        echo "Please provide the file you want uploaded"
    fi

# File download script
elif [[ $1 = get ]]
then
    if [ $# -ge 2 ]
    then
        # If a file name is given it will download it into the current directory
        rsync -avP `grep USERNAME ~/.config/coconet/coconet.conf | cut -d= -f2`@coconet.fun:$2 .
    else
        echo "Please provide the file you want to download"
    fi

# Synchronization scheduler
elif [[ $1 = schedule ]]
then
    # Allows the configurationand reconfiguration of file sync
    echo "Welcome to the Coconet"
    scheduleSync

# Command running script
elif [[ $1 = run ]]
then
    #TODO set up this thing
    echo "it also works"

# SFTP connection
elif [[ $1 = ftp ]]
then
    # Will allow a user to connect via SFTP to view their uploaded files
    echo "Connecting to the Coconet"
    sftp `grep USERNAME ~/.config/coconet/coconet.conf | cut -d= -f2`@coconet.fun

# SSH connection
elif [[ $1 = ssh ]]
then
    # Will allow a user to connect via SSH to manage their system
    echo "Connecting to the Coconet"
    ssh `grep USERNAME ~/.config/coconet/coconet.conf | cut -d= -f2`@coconet.fun

# Coconut Help
elif [[ $1 = help ]]
then
    # DIsplays a helpful help message
    echo "Welcome to the Coconet help page"
    echo "below is a list of commands recognized by Coconet"
    echo -e "\tcoconet install \t\tInstalls and configures the coconet\n\t\t\t\t\tRemoves any existing configuration as well"
    echo -e "\tcoconet schedule \t\tConfigures automatic sync of a selected folder"
    echo -e "\tcoconet put <FILE> <DEST> \tUploads specified file to the specified destination"
    echo -e "\tcoconet get <PATHTOFILE> \tDownloads specified file to the current directory"
    echo -e "\tcoconet run <COMMAND> \t\tRun the given command on the Coconut\n\t\t\t\t\treturn the output if applicable"
    echo -e "\tcoconet ssh \t\t\tOpens a remote connection to the Coconut"
    echo -e "\tcoconet ftp \t\t\tOpens an FTP connection to the Coconut"
    echo -e "\tcoconet help \t\t\tDisplay this useful help message"

# Default
else
    # If a correct command is not found
    echo "Not a recognized command, please try again"

fi
